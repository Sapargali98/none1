function openForm() {
    const formContainer = document.getElementById('form-container');
    formContainer.innerHTML = `
        <form class="form-container">
            <input type="text" placeholder="Имя" name="name" required>
            <input type="email" placeholder="email" name="email" required>
            <input type="tel" placeholder="Телефон" name="phone" required>
            <button type="submit" class="btn">Отправить</button>
        </form>
    `;
}
